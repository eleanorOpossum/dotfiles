" Needed for vundle to work
set nocompatible
filetype off

""" Plugin Management 
" Initialize Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" Load Plugins
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'easymotion/vim-easymotion'
Plugin 'luochen1990/rainbow'
Plugin 'vim-syntastic/syntastic'
Plugin 'myint/syntastic-extras'
Plugin 'rust-lang/rust.vim'
Plugin 'morhetz/gruvbox'
"Plugin 'dracula/vim', { 'name': 'dracula' }

if has('nvim')
	Plugin 'autozimu/LanguageClient-neovim'
	"Plugin 'Shougo/deoplete.nvim'
	"let g:deoplete#enable_at_startup = 1
endif


" Finish Loading Vundle
call vundle#end()
filetype plugin indent on

""" My Options
" allow mouse
set mouse=a

" show cursor position
set ruler

" line numbers
set number

" highlight current line
set cursorline

" syntax highligting
syntax on

" Tab/indent settings
" 4 char width tabs
" Use a tab character
" Don't expand
" Auto-indent newlines
set autoindent
set tabstop=4
set shiftwidth=4
set softtabstop=-1
set noexpandtab

" Never show status
set laststatus=2

" Needed to fix backspacing in certain situations
set bs=2

" Set cursor for different modes
let &t_SI = "\<Esc>[5 q"
let &t_SR = "\<Esc>[3 q"
let &t_EI = "\<Esc>[2 q"

" Reload files on change
set autoread

" Reload file on buffer change
au FocusGained,BufEnter * :checktime

""" Key Bindings
" emacs/readline like bindings in insertmode
map! <C-A> <Home>
map! <C-B> <Left>
map! <C-E> <End>
map! <C-F> <Right>
map! <C-P> <Up>
map! <C-N> <Down>
map! <C-D> <Delete>

" Remap <Leader> to <Space>
let mapleader=" "

""" Plugin-Specific Configuration
""" Set colorscheme
colorscheme gruvbox

""" Airline
let g:airline_theme='gruvbox'
"let g:airline_powerline_fonts = 1

""" rainbow
let g:rainbow_active = 1

""" LanguageClient
set hidden
set signcolumn=yes
let g:LanguageClient_serverCommands = {
	\ 'c': ['clangd'],
	\ 'cpp': ['clangd'],
	\ 'python': ['pyls'],
	\ 'rust': ['rls']
	\}
" Language server bindings
map <Leader>gd <Plug>(LanguageClient#textDocument_definition())<CR>
map <Leader>lh <Plug>(LanguageClient#textDocument_hover())<CR>

""" EasyMotion
" Change easymotion to use a single leader press
map <Leader> <Plug>(easymotion-prefix)

" Navigation
map <Leader>h <Plug>(easymotion-linebackward)
map <Leader>j <Plug>(easymotion-bd-jk)
map <Leader>k <Plug>(easymotion-bd-jk)
map <Leader>l <Plug>(easymotion-lineforward)

" Change word search to bi-directional
map <Leader>w <Plug>(easymotion-bd-w)
map <Leader>W <Plug>(easymotion-bd-W)
map <Leader>e <Plug>(easymotion-bd-e)
map <Leader>E <Plug>(easymotion-bd-E)
