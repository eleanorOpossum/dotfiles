# Defined in /tmp/fish.hWzDtl/psgrep.fish @ line 2
function psgrep
	set header (ps aux | head -n 1 )
	ps aux | rg (string join "" "[" (string sub -l 1 $argv[1]) "]"\
	(string sub -s 2 $argv[1])) \
	| fzf -m --header=$header | echo -n ""
end
