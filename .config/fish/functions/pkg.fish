# Defined in /tmp/fish.8AUAu2/pkg.fish @ line 2
function pkg
         # cmd is first arg, used for yay/pacman/flatpak subcommands
         # args is a list of arguments passed to the subcommand
         set cmd $argv[1]
         set args $argv[2..-1]

         switch $cmd
                case search
                     yay -Slq | fzf -q "$1" -m --preview 'yay -Si {1}' | \
		     xargs -ro yay -S
                case update
                     yay -Syyu
                case install
                     yay -S $args
                case fp-install
                     flatpak install $args
                case '*'
                     echo "$cmd is not a valid command"
        end
end
