function psgrep
	set header (ps aux | head -n 1 )
	ps aux | rg (string join "" "[" (string sub -l 1 $argv[1]) "]"\
	(string sub -s 2 $argv[1])) \
	| fzf -m --header=$header \
	| awk '{print $2}' | rg '\d\d\d\d' | xargs -n1 -d\n kill
end
