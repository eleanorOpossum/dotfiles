function copy --wraps='rsync -aP --remove-sent-files'
rsync -aP $argv
end
