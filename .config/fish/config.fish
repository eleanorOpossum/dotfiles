set -a PATH /home/e/.emacs.d/bin/
set -a PATH /home/e/.cargo/bin/
set -a PATH /home/e/bin/

set -U FZF_DEFAULT_COMMAND "rg --files --hidden"
set -U RUST_SRC_PATH /home/e/src/rust/


fzf_key_bindings
