# Modules to import automagically when the REPL starts

# Import things that are in cpython/Lib outside of the main try/crach
import re 
import os
import sys
import math 
import json
import shlex
import asyncio
import requests
import subprocess as sp

# Import only datetime.datetime
from datetime import datetime

# Clears the screen; personal preference
_ = os.system('clear')

# List to store all not found modules
__notfound = []

# Try/catch for non std imports
try:
    import pandas as pd
except ModuleNotFoundError as nf:
    __notfound.append(nf)
    pass

try:
    import numpy as np
except ModuleNotFoundError as nf:
    __notfound.append(nf)
    pass

try:
    import matplotlib
except ModuleNotFoundError as nf:
    __notfound.append(nf)
    pass

try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError as nf:
    __notfound.append(nf)
    pass

try:
    import networkx as nx
except ModuleNotFoundError as nf:
    __notfound.append(nf)
    pass

try:
    import scipy
except ModuleNotFoundError as nf:
    __notfound.append(nf)
    pass

# My special functions

# Read a file to a string
def read_file(filename):
    with open(filename, 'r') as f:
        return f.read()


# Execute code from a file
def exec_file(filename):
    code = read_file(filename)

    try:
        exec(code)
    except Exception as e:
        print(e)


def number_string(string, reach, prefix=False):
    if prefix:
        return list(map(lambda x: str(x) + string, reach))
    else:
        return list(map(lambda x: string + str(x), reach))


# List all the modules I imported because I forget what I have sometimes
print("Imported Modules:", end=" ")

first_import = True

# List only things explicitly imported and their dependancies
for imp in dir():
    if "__" not in imp and "_" != imp and "first_import" != imp:
        if not first_import:
            # Print not the first import with a prefix
            print("," + imp, end="")
        else:
            # Print nothing before the first import
            print(imp, end="")
            first_import = False
# Newline
print("")

if __notfound:
    for nf in __notfound:
        print(nf)
