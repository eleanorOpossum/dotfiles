#!/bin/bash

CMD=$(date '+%a %d %b %H:%M %Y ')

HOUR=$(date '+%H')

CLOCK=""

case $HOUR in
	0 | 12)
	CLOCK="🕛"
	;;
	1 | 13)
	CLOCK="🕐"
	;;
	2 | 14)
	CLOCK="🕑"
	;;
	3 | 15)
	CLOCK="🕒"
	;;
	4 | 16)
	CLOCK="🕓"
	;;
	5 | 17)
	CLOCK="🕔"
	;;
	6 | 18)
	CLOCK="🕕"
	;;
	7 | 19)
	CLOCK="🕖"
	;;
	8 | 20)
	CLOCK="🕗"
	;;
	9 | 21)
	CLOCK="🕘"
	;;
	10 | 22)
	CLOCK="🕙"
	;;
	11 | 23)
	CLOCK="🕚"
	;;
esac

echo "$CLOCK" "$CMD"
