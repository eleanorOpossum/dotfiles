#!/usr/bin/env bash

GPG_TTY=($tty)
if [$# -lt 2]
then
	echo "usage: encypt.sh ARCHIVE_NAME FILE_1 [FILE_2, ... FILE_N]"
fi

ARCHIVE_NAME=$1

shift

tar -cvzf - $@ | gpg --symmetric --cipher-algo aes256 -o $ARCHIVE_NAME

