#!/usr/bin/env python3

from shutil import get_terminal_size
from math import floor
import os

def print_stripe(color, string):
  end  = u"\u001b[0m" # ANSI color reset
  print(color, string, end, sep="", end="\n", flush=True)
  
def transrights():
  # ANSI color codes
  blue = u"\u001b[36;1m"
  pink = u"\u001b[35;1m"
  whit = u"\u001b[37;1m"

  size = get_terminal_size((80, 20))

  # leading space for all stripes
  stripe = " "

  # columns - 2 is used along with the leading space for horizontal centering
  for i in range(0, size.columns - 2):
     stripe += chr(9608) #9068d is unicode full block

  # floor is used since '/' returns a float and flooring the result prevents
  # the flag from being drawn off screen.
  stripe_height = floor(size.lines / 6)

  temp = stripe
  for i in range(0, stripe_height):
    stripe += "\n"
    stripe += temp

  # clears the screen
  _ = os.system("clear")

  print() # <-- might cause rendering issues on small heights
  print_stripe(blue, stripe)
  print_stripe(pink, stripe)
  print_stripe(whit, stripe)
  print_stripe(pink, stripe)
  print_stripe(blue, stripe)
  print(u"\u001b[0m") # reset the colors again for safety
  
transrights()
