#!/bin/sh

unzip Blind_Mr._Jones_-_Stereo_Musicale_Retrospective.zip -d Blind_Mr._Jones/Stereo_Musicale_Retrospective/
unzip Dott_-_Heart_Swell.zip -d Dott/Heart_Swell/
unzip Graveface_Records_&_Curiosities_-_Dott_&_Night_School_-_Carousel.zip -d Graveface_Records_&_Curiosities_-_Dott_&_Night_School/Carousel/
unzip Night_School_-_Blush.zip -d Night_School/Blush/
unzip Night_School_-_Disappear_Here.zip -d Night_School/Disappear_Here/
unzip Night_School_-_Night_School_-_Heart_Beat.zip -d Night_School_-_Night_School/Heart_Beat/
unzip Rude_Dude_and_the_Creek_Freaks_-_s-t.zip -d Rude_Dude_and_the_Creek_Freaks/s-t/
unzip Serengeti_+_Sicker_Man_-_Doctor_My_Own_Patience.zip -d Serengeti_+_Sicker_Man/Doctor_My_Own_Patience/
unzip Skippy_Spiral_-_Circuit_Circus.zip -d Skippy_Spiral/Circuit_Circus/
unzip Sondra_Sun-Odeon_-_Desyre.zip -d Sondra_Sun-Odeon/Desyre/
unzip Tennis_System_-_Lovesick.zip -d Tennis_System/Lovesick/
unzip Tennis_System_-_P_A_I_N.zip -d Tennis_System/P_A_I_N/
unzip The_Stargazer_Lilies_-_Lost.zip -d The_Stargazer_Lilies/Lost/
unzip Valley_Gals_-_Real_Rock.zip -d Valley_Gals/Real_Rock/
unzip Xiu_Xiu_-_A_Promise_(Expanded).zip -d Xiu_Xiu/A_Promise_(Expanded)/
unzip Xiu_Xiu_-_Knife_Play.zip -d Xiu_Xiu/Knife_Play/
