#!/usr/bin/env bash
echo "Checking distro..."

echo "Checking if required packages exist..."

echo "Installing normal packages + required..."

echo "Installing configs..."
mkdir ~/.config
cp -r config/* ~/.config/
cp tmux.conf ~/.tmux.conf
cp -r vim ~/.vim

echo "Installing 'Dracula' GTK theme..."
mkdir -p ~/.themes/Dracula-GTK-TEST/
cd ~/.themes/Dracula-GTK-TEST/
wget https://github.com/dracula/gtk/archive/master.zip
unzip -qq master.zip
rm master.zip
mv */* ./
rm -r gtk-master

echo "Install 'Dracula' Gnome Terminal theme..."
echo "(REQUIRES USER INTERACTION!)"
mkdir temp
cd temp
git clone https://github.com/dracula/gnome-terminal
cd gnome-terminal
./install.sh
cd ../..
yes | rm -r temp


