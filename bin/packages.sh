#!/usr/bin/env bash

# This should be called from the 'install.sh' script

# Packages I always install from the package manager
BASE="fish ripgrep neovim emacs tmux flatpak git curl wget"
GUI="firefox pluma keepassxc gpick gimp"
GNOME="gnome-tweak-tool"
EXTRA="neofetch"
FLATPAK="discord spotify"

# Something to handle distro detection and changing packages/names based on 
# the current distro. (e.g. install yay for Arch, s/rename/prename/g on RPM)

# Install order:
# Base --> GUI --> GNOME --> EXTRA --> FLATPAK
#     \--> GIT_PKGS
# probably not gonna be parallel, but...

# Special stuff needed for each
GIT_PKGS="ponysay lolcat doomemacs"

# Need Rustup and chrome somewhere in here...
# Should I like to static 
# Thing to append binaries to $PATH goes here
