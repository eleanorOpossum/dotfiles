#!/bin/bash

CMD=""

for SINK in `pactl list sinks | grep "Sink #" | cut -b7-`
do
	case $1 in
		i)
			pactl set-sink-volume $SINK +5%
			;;
		d)
			pactl set-sink-volume $SINK -5%
			;;
		m)
			pactl set-sink-mute $SINK toggle
			;;
	esac		
			
done
