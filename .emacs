;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; .emacs
;;; There are many like it, but this one is mine
;;;
;;; Todo:
;;; - Configure Windmove so I don't need to use Evil's C-w chords
;;; - Figure out what the hell helm does
;;; - Find and install a folding plugin?
;;; - Fix centaur-tab colors
;;; - install moar packages

;;----------------------------------------------------------------------------  
;; Customize Standard emacs things
;;----------------------------------------------------------------------------  

;; Set Font Size to 11pt
(set-face-attribute 'default nil :height 110)

;; Disable the stupid menubar and toolbar
(menu-bar-mode -1)
(tool-bar-mode -1)

;; Line and Column numbers
(require 'display-line-numbers)
(defcustom display-line-numbers-exempt-modes '(vterm-mode)
  "Major modes to disable linum-mode in"
  :group 'display-line-numbers
  :type 'list
  :version "green")

;; Apply to initializing linum-mode
(defun display-line-numbers--turn-on ()
  "Turn on linum-mode *except* when in certain major modes"
  (if (and
       (not (member major-mode display-line-numbers-exempt-modes))
       (not (minibufferp)))
      (display-line-numbers-mode)))

(global-display-line-numbers-mode)

(setq column-number-mode t)

;; Windmove mode
(windmove-default-keybindings)
(global-set-key (kbd "C-c h") 'windmove-left)
(global-set-key (kbd "C-c l") 'windmove-right)
(global-set-key (kbd "C-c k") 'windmove-up)
(global-set-key (kbd "C-c j") 'windmove-down)

;; Don't create backup files in PWD
(setq backup-directory-alist `(("." . "~/.autosaves")))

;; Add ~/.local/bin to $PATH so LSP-mode doesn't break
(add-to-list 'exec-path "~/.local/bin/")

;;----------------------------------------------------------------------------  
;; Package Management
;;----------------------------------------------------------------------------

;; Built-in Packages
;;---------------------------------------------------------------------------- 
(require 'uniquify)
(require 'package)
(require 'ido)
(ido-mode t)

;; Add repos to package.el
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

(setq package-enable-at-startup nil)

;; Install use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))

;; Non Built-in Packages
;;-----------------------------------------------------------------------------

;; Thing Components -----------------------------------------------------------
(use-package popup :ensure t)
(use-package async :ensure t)
(use-package helm  :ensure t)
(use-package restart-emacs :ensure t)
(use-package vterm
  :ensure t
  :init
  (add-hook 'vterm-exit-hook (lambda ()
			       (let* ((buffer (current-buffer))
				      (window (get-buffer-window buffer)))
				 (when window
				   (delete-window window))
				 (kill-buffer buffer))))
  :config
  (setq vterm-kill-buffer-on-exit t))

;; Code Completion ------------------------------------------------------------
(use-package lsp-mode
  :ensure t
  :init
  (setq lsp-keymap-prefix "C-c l")
  :hook
  (lsp-mode . lsp-enable-which-key-integration)
  :commands lsp lsp-deferred)

(use-package company
  :ensure t
  :after lsp-mode
  :init
  (add-hook 'after-init-hook 'global-company-mode))

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
			 (require 'lsp-pyright)
			 (lsp-deferred))))

;; Themes and Appearance ------------------------------------------------------
(use-package dracula-theme
  :ensure t
  :init
  (load-theme 'dracula t))

(use-package doom-modeline
  :ensure t
  :init
  (doom-modeline-mode 1))

;; (use-package centaur-tabs
;;   :ensure t
;;   :config
;;   (setq centaur-tabs-style "bar"
;; 	centaur-tabs-set-bar 'left
;; 	centaur-tabs-height 32
;; 	centaur-tabs-set-icons t
;; 	centaur-tabs-gray-out-icons 'buffer
;; 	centaur-tabs-set-modified-marker t
;; 	centaur-tabs-set-close-button nil)
;;   (centaur-tabs-mode t))


;; (use-package highlight-indent-guides
;;   :ensure t
;;   :init
;;   (setq highlight-indent-guids-method 'bitmap)
;;   (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))

(use-package indent-guide
  :ensure t
  :config
  (indent-guide-global-mode))

(use-package vi-tilde-fringe
  :ensure t
  :config
  (setq global-vi-tilde-fringe-mode t))

;; Evil Mode and Plugins ------------------------------------------------------
(use-package evil
  :ensure t
  :init
  (setq evil-disable-insert-state-bindings t)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (setq evil-insert-state-cursor '(bar "#f8f8f2")
	evil-normal-state-cursor '(box "#f8f8f2")
	evil-visual-state-cursor '(box "#bd93f9"))
  (evil-set-initial-state 'vterm-mode 'emacs))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package evil-easymotion
  :after evil
  :ensure t
  :config
  (evilem-default-keybindings "SPC"))

;; Fancy Things ---------------------------------------------------------------
(use-package magit :ensure t)

(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map))

(use-package browse-kill-ring
  :ensure t
  :init
  (browse-kill-ring-default-keybindings))

(use-package ace-jump-mode
  :ensure t
  :config
  (define-key global-map (kbd "C-c SPC") 'ace-jump-mode))

(use-package smex
  :ensure t
  :init
  (smex-initialize)
  :config
  (global-set-key (kbd "M-x") 'smex)
  (global-set-key (kbd "M-X") 'smex-major-mode-commands)
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command))

(use-package which-key
  :ensure t
  :init
  (which-key-mode)
  :config
  (setq which-key-popup-type 'minibuffer))

;; Modes for Coding -----------------------------------------------------------
(use-package elpy
  :ensure t
  :init
  (elpy-enable))


(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package markdown-mode+ :ensure t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(delete-selection-mode nil)
 '(helm-minibuffer-history-key "M-p")
 '(package-selected-packages
   '(company-lsp lsp-mode indent-guide vi-tilde-fringe markdown-mode+ ## grip-mode emacs-which-key restart-emacs smex ace-jump-mode vterm helm async popup centaur-tabs evil-easymotion highlight-indent-guides elpy evil-collection evil-visual-mark-mode))
 '(tab-bar-close-button-show nil)
 '(tab-bar-close-last-tab-choice 'delete-frame)
 '(tab-bar-mode t)
 '(tab-bar-new-button-show nil)
 '(tab-bar-show 1)
 '(tab-bar-tab-hints t)
 '(tab-bar-tab-name-function 'tab-bar-tab-name-current-with-count)
 '(uniquify-buffer-name-style 'forward nil (uniquify)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(tab-bar ((t (:inherit variable-pitch :background "#44475a" :foreground "#f8f8f2"))))
 '(tab-bar-tab ((t (:background "#282a36" :foreground "#f8f8f2" :box (:line-width 2 :color "#282a36"))))))
