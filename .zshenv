export PYTHONSTARTUP=~/.python_startup.py
export GPG_TTY=$(tty)

# Add user bin dirs to $PATH if they exist
if [[ -d "$HOME/bin" ]]
then
	export PATH="$HOME/bin:$PATH"
fi

if [[ -d "$HOME/bin/platform-tools" ]]
then
	export PATH="$HOME/bin/platform-tools:$PATH"
fi

if [[ -d "$HOME/.local/bin" ]]
then
	export PATH="$HOME/.local/bin:$PATH"
fi

if [[ -d "$HOME/.emacs.d/bin" ]]
then
	export PATH="$HOME/.emacs.d/bin:$PATH"
fi

if [[ -d "/usr/lib64/mpi/gcc/mpich/bin" ]]
then
	export PATH="/usr/lib64/mpi/gcc/mpich/bin:$PATH"
fi

if [[ -f /usr/bin/nvim ]]
then
	export EDITOR="/usr/bin/nvim"
elif [[ -f /usr/bin/vim ]]
then
	export EDITOR="/usr/bin/vim"
elif [[ -f /bin/vim ]]
then
	export EDITOR="/bin/vim"
elif [[ -f /usr/bin/vi ]]
then
	export EDITOR="/usr/bin/vi"
elif [[ -f /bin/vi ]]
then
	export EDITOR="/bin/vi"
fi

bindkey -e

# If cargo is installed, source rust things
if [[ -d "$HOME/.cargo" ]]
then
	source "$HOME/.cargo/env"
	export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"
fi
export ParaView_DIR="$HOME/src/paraview_build/"
export LD_LIBRARY_PATH="$HOME/src/paraview_build/lib64:$LD_LIBRARY_PATH"
